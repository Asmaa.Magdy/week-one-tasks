//  Fourth Task
//  nested array and nested object copy them and try to change in first level and nested level

let numArray = [
    [1, 2, 3],
    [4, 5, 6],
    [7, 8, 9],
];
console.log('Fourth Task');

let newShallowArray = [...numArray];
newShallowArray[1][1] = 55;
console.log(`Shallow Copy ${numArray}`);
console.log(`Shallow Copy ${newShallowArray}`);

console.log('---------------');

let newDeepArray = JSON.parse(JSON.stringify(numArray));
newDeepArray[0][0] = 111;
console.log(`Deep Copy ${numArray}`);
console.log(`Deep Copy ${newDeepArray}`);



let person = {
    data: {
        name: 'asmaa',
        id: 122,
        email: 'a@a.com',
    },
};

let newPerson = JSON.parse(JSON.stringify(person));
newPerson.data.name = 'amira';
console.log(person.data);
console.log(newPerson.data);

console.log('---------------');

let newDeepPerson = { ...person };
newDeepPerson.data.email = 'aaaaaaa@a.com';
console.log(person.data);
console.log(newDeepPerson.data);

//  Fourth Task
//  nested array and nested object copy them and try to change in first level and nested level

let numArrayF = [1, 2, 3];

console.log('Fourth Task');

let newShallowArrayF = [...numArrayF];
newShallowArrayF[1] = 55;
console.log(`Shallow Copy ${numArrayF}`);
console.log(`Shallow Copy ${newShallowArrayF}`);

console.log('---------------');

let newDeepArrayF = JSON.parse(JSON.stringify(numArrayF));
newDeepArrayF[0] = 111;
console.log(`Deep Copy ${numArrayF}`);
console.log(`Deep Copy ${newDeepArrayF}`);

let personF = {
    name: 'asmaa',
    id: 122,
    email: 'a@a.com',
};

let newPersonF = JSON.parse(JSON.stringify(personF));
newPersonF.name = 'amira';
console.log(personF);
console.log(newPersonF);

console.log('---------------');

let newDeepPersonF = { ...personF };
newDeepPersonF.email = 'aaaaaaa@a.com';
console.log(personF);
console.log(newDeepPersonF);
