// First Task
// merged  arrays and removed duplicated using spread operators

let array1 =  [1, 2, 3];
let array2 =  [3, 4, 5];
let array3 =  [5, 6, 7];
let newArray = new Set ( [...array1 , ...array2 , ...array3]) 
console.log("First Task");
console.log(...newArray);

// Second Task
// function calculate average for any numbers i will pass in params by rest operators

function average( ...numbers) {
    let sum = 0
    numbers.forEach( number => {
        sum =  sum +  number 
    });
    let average = sum/ numbers.length
    return average
    
}
console.log("Second Task");
console.log(average(1, 2 , 3 ,4));

// Third Task
// factorial 
function fact( num) {
    if( num === 0 ||  num === 1 ){
    return 1
    }else{
        return num * fact(num-1) } 
}
console.log("Third Task");
console.log(fact(5));

